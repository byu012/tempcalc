package com.example.mycalc

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mockito
import java.lang.IllegalStateException

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
//    @Before
//    fun setUp() {
//        val p : PermissionCheck = mock(PermissionCheck::class.java)
//
//        assertTrue(p != null)
//    }

    @Test
    fun permission_check() {
        val p : PermissionCheck = Mockito.mock(PermissionCheck::class.java)

        Mockito.`when`(p.isInitialize(0)).thenReturn(true)

        val manager = PermissionManager(p)
        manager.initPermission(0)

        Mockito.verify(p).isInitialize(0)

        

//        assertEquals(p.isInitialize(0), true)
//        assertEquals(p.isInitialize(-1), false)

//        Mockito.verify(p).isInitialize(-1)
//        assertTrue(p != null)
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
